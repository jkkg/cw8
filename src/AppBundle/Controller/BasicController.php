<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_user_profile', array(

            ));
        }
        return $this->render('AppBundle:Basic:index.html.twig', array(
            // ...
        ));
    }

}
