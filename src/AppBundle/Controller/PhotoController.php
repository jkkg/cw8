<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\AddPhotoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PhotoController extends Controller
{
    /**
     * @Route("/addPhoto")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPhotoAction(Request $request)
    {
        $photo = new Photo();

        $form = $this->createForm(addPhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $photo->setAuthor($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }

        return $this->render('@App/Photo/add_photo.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/addComment")
     */
    public function addCommentAction()
    {
        return $this->render('@App/Photo/add_comment.html.twig', array(
            // ...
        ));
    }



}
