<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/profile")
     */
    public function profileAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Photo[] */
        $photos = $user->getPhotos();

        return $this->render('AppBundle:User:profile.html.twig', array(
            'Username' => $user->getUsername(),
            'photos' => $photos
        ));
    }

    /**
     * @Route("/edit")
     */
    public function editAction()
    {
        return $this->redirectToRoute('fos_user_profile_edit', array(
            // ...
        ));
    }

}
